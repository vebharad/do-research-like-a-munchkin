# Do Research Like a Munchkin - Team 1

Shared repository for the [Do Research Like a Munchkin](https://the-munchkins.gitlab.io/do-research-like-a-munchkin)
programming course.

For a detailed overview of our work and findings on the research project,
visit https://munchkins-team-1.gitlab.io/do-research-like-a-munchkin.
