#!/usr/bin/env python3
import numpy as np
import time
from typing import Callable, List, Tuple, Union, Optional
from inspect import signature
from copy import deepcopy
from sklearn.cluster import KMeans

from particle_generator import ParticleGenerator

# NDimPoint = np.typing.ArrayLike # For when numpy 1.21 releases
NDimPoint = Union[float, List[float], Tuple[float, ...], np.ndarray]
"""An n-dimensional point which can be converted to a NumPy array of shape `(n,)`"""


class BestPairsFinder:
    def __init__(self, f: Callable[[NDimPoint, NDimPoint], float]):
        """Find best pairs of points to minimize a function f.

        Args:
            f: Function that returns float for two points. Arguments may be
                single floats, lists of floats, or one-dimensional NumPy arrays.

        Raises:
            TypeError: If `f` is not a function.
            ValueError: If `f` does not take exactly two arguments.
        """
        if not callable(f):
            raise TypeError("f is not callable!")

        if len(signature(f).parameters) != 2:
            raise ValueError("f must take exactly two arguments!")

        self.f = f

        self._points = None
        self.pair_mat = None
        self.n_points = None

    @property
    def points(self):
        """List of NDimPoints"""
        return self._points

    @points.setter
    def points(self, l_points: List[NDimPoint]):
        """
        Args:
            l_points: List of n-dimensional points

        Raises:
            ValueError: If number of points is odd.
        """
        if len(l_points) % 2 != 0:
            raise ValueError("The number of points is not even.")

        self._points = l_points
        self.n_points = len(l_points)
        self.pair_mat = self.func_matrix()

    @points.deleter
    def points(self):
        del self._points

    @staticmethod
    def distance(point1: NDimPoint, point2: NDimPoint) -> float:
        """Calculates the euclidian distance between two points in
        n-dimensional space.

        Args:
            point1: float, 1D list, or 1D NumPy array
            point2: float, 1D list, or 1D NumPy array

        Returns:
            Distance between `point1` and `point2`.

        Raises:
            ValueError: If points are not 1 column vectors.
            ValueError: If points do not have the same dimensionality.

        Examples:
            Distance between 1D-points, represented as floats:
            >>> BestPairsFinder.distance(2, 3)
            1

            Distance between 2D-points:
            >>> BestPairsFinder.distance((0, 0), (1, 1))
            1.4142135623730951
        """
        p1 = np.asarray(point1)
        p2 = np.asarray(point2)

        if len(p1.shape) > 1:
            raise ValueError("Point1 is not a vector!")

        if len(p2.shape) > 1:
            raise ValueError("Point2 is not a vector!")

        if p1.shape != p2.shape:
            raise ValueError("The points do not have the same dimensionality!")

        return np.linalg.norm(p1 - p2)

    def func_matrix(self) -> np.ndarray:
        """ Calculates a matrix of the function `f` for all pairs in points.

        Returns:
            Matrix that is filled with values of f for pairs.

        Raises:
            RuntimeError: If called before points have been set.

        Examples:
            Distance matrix for two 1D points:
            >>> finder = BestPairsFinder(BestPairsFinder.distance)
            >>> finder.points = [2, 3]
            >>> finder.func_matrix()
            array([[0., 1.],
                   [1., 0.]])
        """
        if self._points is None:
            raise RuntimeError("points have not yet been set!")

        dist_mat = np.zeros(shape=(self.n_points, self.n_points), dtype=np.float64)

        for i in range(self.n_points):
            for j in range(i + 1, self.n_points):
                dist_mat[i, j] = self.f(self._points[i], self._points[j])
                dist_mat[j, i] = dist_mat[i, j]
        return dist_mat

    def find_pairs(self) -> Tuple[List[Tuple[int, int]], float]:
        """Returns a list of pairs of points that minimizes the sum of f(p1, p2).

        Note:
            This algorithm is only guaranteed to provide a true minimum
            for 1D points; for higher dimensions, the result will generally
            not be minimal, but close to the true minimum. Also, only one
            solution is returned, even if there are multiple possible
            pairings to achieve it.

        Returns:
            Tuple containing the list of optimal pairs, and the corresponding target function.

        Raises:
            RuntimeError: If called before points have been set.

        Examples:
            Trivial example for two 1D points:
            >>> finder = BestPairsFinder(BestPairsFinder.distance)
            >>> finder.points = [2, 3]
            >>> finder.find_pairs()
            ([(0, 1)], 1.0)

            When there are multiple solutions, only one of them is returned:
            >>> finder = BestPairsFinder(BestPairsFinder.distance)
            >>> finder.points = [(1, 1), (-1, 1), (-1, -1), (1, -1)]
            >>> finder.find_pairs()
            ([(0, 1), (2, 3)], 4.0)
        """
        if self._points is None:
            raise RuntimeError("points have not yet been set!")

        if self.n_points == 0:
            return [], 0.0

        list_of_pairs = []
        mask = np.ma.asarray(self.pair_mat)

        line_sums = np.sum(mask, axis=1)

        for i in range(self.n_points):
            mask[i, i] = np.ma.masked

        while mask.all() is not np.ma.masked:
            # Find the row with the highest sum (which has not been masked)
            ind_max_line = line_sums.argmax(fill_value=0)

            # Find the smallest element within that row
            ind_min_elem = mask[ind_max_line].argmin(fill_value=np.inf)

            list_of_pairs.append((min(ind_max_line, ind_min_elem), max(ind_max_line, ind_min_elem)))

            # Delete the corresponding rows and columns...
            mask[ind_max_line, :] = np.ma.masked
            mask[ind_min_elem, :] = np.ma.masked
            mask[:, ind_min_elem] = np.ma.masked
            mask[:, ind_max_line] = np.ma.masked

            # ... and delete the row from the sums to be checked
            # TODO: Should we re-compute the sums here?
            line_sums[ind_max_line] = np.ma.masked
            line_sums[ind_min_elem] = np.ma.masked

        # Sort the list by the pairs' first elements
        return sorted(list_of_pairs, key=lambda pair: pair[0]), self.evaluate_pairing(list_of_pairs)

    def find_pairs_recursive(self, lowest_guess: float = np.inf) -> Tuple[List[List[Tuple[int, int]]], float]:
        r"""Returns a list of all possible pairings of points that minimize the
        sum of f(p1, p2), as well as the corresponding sum.

        Args:
            lowest_guess: float that was obtained from a cheaper method,
                increases speed of the exact method.

        Note:
            While this version is guaranteed to provide the exact minimum, its
            \(\mathcal{O}\left( \frac{n!}{(n/2)! \cdot 2^{n/2}} \right)\) scaling
            behavior makes it take very long very quickly. Running with more
            than 20 particles at your own risk!

        Returns:
            Tuple containing a list of optimal pairings, and the
            corresponding target function.

        Raises:
            RuntimeError: If called before points have been set.

        Examples:
            Trivial example for two points in 1D:
            >>> finder = BestPairsFinder(BestPairsFinder.distance)
            >>> finder.points = [2, 3]
            >>> finder.find_pairs_recursive()
            ([[(0, 1)]], 1.0)

            All solutions leading to the minimal sum will be returned:
            >>> finder = BestPairsFinder(BestPairsFinder.distance)
            >>> finder.points = [(1, 1), (-1, 1), (-1, -1), (1, -1)]
            >>> finder.find_pairs_recursive()
            ([[(0, 1), (2, 3)], [(0, 3), (1, 2)]], 4.0)
        """
        if self._points is None:
            raise RuntimeError("points have not yet been set!")

        if self.n_points == 0:
            return [[]], 0.0

        def helper(remaining_indices: List[int],
                   current_pairing: List[Tuple[int, int]],
                   current_sum: float,
                   current_best_pairing: List[List[Tuple[int, int]]],
                   current_min_sum: float) -> Tuple[List[List[Tuple[int, int]]], float]:
            n_rem = len(remaining_indices)
            if n_rem == 0:
                if current_sum == current_min_sum:
                    return current_best_pairing + [current_pairing], current_min_sum
                return [current_pairing], current_sum

            for i in range(1, n_rem):
                pair = (min(remaining_indices[0], remaining_indices[i]), max(remaining_indices[0], remaining_indices[i]))

                if current_sum + self.pair_mat[pair] > current_min_sum:
                    continue

                current_best_pairing, current_min_sum = helper(remaining_indices[1:i] + remaining_indices[i + 1:],
                                                               current_pairing + [pair],
                                                               current_sum + self.pair_mat[pair],
                                                               current_best_pairing,
                                                               current_min_sum)

            return current_best_pairing, current_min_sum

        # Sort the particles by the sum of their distances to all other
        # particles, starting with the smallest. This way, the helper
        # function will abort quicker.
        sorted_indices = np.argsort(self.pair_mat.sum(axis=1))[::-1].tolist()
        pairings, target_function = helper(remaining_indices=sorted_indices,
                                           current_pairing=[],
                                           current_sum=0.0,
                                           current_best_pairing=[],
                                           current_min_sum=lowest_guess)

        return [sorted(pairing, key=lambda pair: pair[0]) for pairing in pairings], target_function

    def evaluate_pairing(self, pairing: List[Tuple[int, int]]) -> float:
        """Calculates the target function for a given pairing of points."""
        return sum(self.pair_mat[pair] for pair in pairing)

    @staticmethod
    def metropolis(prev: float, new: float, T: float = 1) -> bool:
        """Evaluates whether or not a new guess will be accepted according to
        the Metropolis criterion.
        """
        return new < prev or np.exp(-(new - prev)/T) > np.random.random()

    @staticmethod
    def swap_entries(pairing: List[Tuple[int, int]],
                     first: Optional[Tuple[int, int]] = None,
                     second: Optional[Tuple[int, int]] = None) -> Tuple[Tuple[int, int], Tuple[int, int]]:
        """Randomly swaps two entries in the input list. The indices of
        the swapped entries are returned in case the swap needs to be
        undone if the new guess is not accepted.

        Args:
            pairing: List of tuples of indices for the current pairing.
            first: Tuple containing the index of the first pair and the
                index of the point inside the pair. By default `None`,
                in which case it is randomly generated.
            second: Tuple containing the index of the second pair and the
                index of the point inside the pair. By default `None`,
                in which case it is randomly generated.

        Returns:
            The indices of the swapped entries as tuples.

        Raises:
            IndexError: If the pair or point indices are invalid.
        """
        n_pairs = len(pairing)

        if first is not None and second is not None:
            pair_1, point_1 = first
            pair_2, point_2 = second

            if (pair_1 < 0
                or pair_1 >= n_pairs
                or pair_2 < 0
                or pair_2 >= n_pairs
                ):
                raise IndexError("Pair indices must be between 0 and n_pairs - 1.")
            if (point_1 < 0
                or point_1 >= 2
                or point_2 < 0
                or point_2 >= 2
                ):
                raise IndexError("Point indices must be 0 or 1.")
        else:
            pair_1 = np.random.randint(n_pairs)
            while (pair_2 := np.random.randint(n_pairs)) == pair_1:
                pass

            point_1 = np.random.randint(2)
            point_2 = np.random.randint(2)

        # Conserve the ordering of the tuples
        tmp_1 = [0, 0]
        tmp_1[point_1] = pairing[pair_2][point_2]
        tmp_1[point_1 ^ 1] = pairing[pair_1][point_1 ^ 1]

        tmp_2 = [0, 0]
        tmp_2[point_2] = pairing[pair_1][point_1]
        tmp_2[point_2 ^ 1] = pairing[pair_2][point_2 ^ 1]

        pairing[pair_1] = tuple(tmp_1)
        pairing[pair_2] = tuple(tmp_2)

        return (pair_1, point_1), (pair_2, point_2)

    def pairs_MC(self, curr_pairing: List[Tuple[int, int]], T: float, n_moves: int = 0) -> Tuple[List[Tuple[int, int]], float]:
        """Returns a list of pairs of points that minimizes the sum of f(p1, p2)

        Args:
            curr_pairing: List of touples that contains the current pairing
            T: "Temperature" of the sampling
            n_moves: number of MC moves to be performed

        Returns:
            Tuple containing the list of optimal pairs, and the corresponding target function.

        Raises:
            ValueError: If T < 1.
            ValueError: If n_moves is not an integer or less than 1
            ValueError: If n_moves has not been set

        Examples:
            Trivial example in 1D:
            >>> finder = BestPairsFinder(BestPairsFinder.distance)
            >>> finder.points = [2, 3]
            >>> guess, _ = finder.find_pairs()
            >>> finder.pairs_MC(guess)
            ([(0, 1)], 1.0)
        """
        if T < 0.0:
            raise ValueError("Temperature is below 0. It will break the Metropolis criterion.")
        if n_moves < 1:
            raise ValueError("Given number of moves is too small.")

        lowest_pairing = deepcopy(curr_pairing)
        lowest_estimate = self.evaluate_pairing(curr_pairing)
        curr_estimate = lowest_estimate
        loops_wo_change = 0

        while loops_wo_change < n_moves:
            loops_wo_change += 1

            first, second = self.swap_entries(curr_pairing)
            new_estimate = self.evaluate_pairing(curr_pairing)

            if self.metropolis(prev=curr_estimate, new=new_estimate, T=T):
                curr_estimate = new_estimate

                if curr_estimate < lowest_estimate:
                    lowest_pairing[:] = curr_pairing[:]
                    lowest_estimate = curr_estimate
                    # loops_wo_change = 0
            else:
                self.swap_entries(curr_pairing, first, second)

        return sorted(lowest_pairing, key=lambda pair: pair[0]), lowest_estimate

    def get_even_sized_clusters(self, n_clusters: int) -> List[List[NDimPoint]]:
        """Cluster `NDimPoints` based on their euclidean distance with
        the KMeans algorithm into clusters of even size.

        Note:
            The number of clusters may be altered to handle clusters
            of uneven size.

        Args:
            n_clusters: Lower bound for the number of clusters to form
                as well as the number of centroids to generate with kmeans.

        Returns:
            List of even sized clusters of points.

        Examples:
            Trivial example in 1D:
            >>> finder = BestPairsFinder(BestPairsFinder.distance)
            >>> finder.points = [1, 2, 3, 4, 5, 6]
            >>> clusters = finder.get_even_sized_clusters(2)
            >>> clusters
            [[[1], [2]], [[4], [3]], [[5], [6]]]

            Note that even though we specified two clusters, the algorithm
            produced three clusters of size 2 (otherwise, we would have
            gotten two clusters of size 3).
        """
        # get index of cluster for each point and fitted KMeans model
        cluster, km = self.kmeans(n_clusters)

        if cluster is None and km is None:
            return []

        # store points sorted by cluster
        uneven_clusters = []
        centers_uneven_cluster = []
        even_clusters = []

        cluster = np.array(cluster)
        # To handle 1D points...
        if not hasattr(self._points[0], '__len__'):
            points = np.array(self._points).reshape(-1, 1)
        else:
            points = np.array(self._points)

        for i in range(n_clusters):
            mask = cluster == i
            if len(points[mask]) % 2 == 0:
                even_clusters += [points[mask]]
            else:
                uneven_clusters += [points[mask]]
                centers_uneven_cluster += [km.cluster_centers_[i]]

        # handle clusters of uneven size
        if uneven_clusters:
            # pair cluster centers
            # TODO: We should be able to use any metric here...
            best_center_pairs = BestPairsFinder(BestPairsFinder.distance)
            best_center_pairs.points = centers_uneven_cluster

            # TODO: Should we also use the recursive or MC pairing here?
            paired_clusters, _ = best_center_pairs.find_pairs()

            # get clusters with even number of elements
            for c in paired_clusters:
                # get distance of all points of paired cluster
                # TODO: Same as above, when we implement different metrics for
                # K-Means, np.linalg.norm should be replaced
                d0 = np.linalg.norm(uneven_clusters[c[0]] - centers_uneven_cluster[c[1]], axis=1)
                d1 = np.linalg.norm(uneven_clusters[c[1]] - centers_uneven_cluster[c[0]], axis=1)

                # get index of minimal elements
                ind = np.argmin(d0), np.argmin(d1)

                # add closest pair of points and remaining cluster to even clusters
                even_clusters += [np.array([uneven_clusters[c[0]][ind[0]], uneven_clusters[c[1]][ind[1]]])]

                # remove the points from the uneven cluster and check if any points remain
                for i in range(2):
                    new_even = np.array([*uneven_clusters[c[i]][:ind[i]], *uneven_clusters[c[i]][ind[i] + 1:]])
                    if new_even.size > 0:
                        even_clusters += [new_even]

        # convert list of np.ndarrays to list of lists
        for i, cluster in enumerate(even_clusters):
            even_clusters[i] = cluster.tolist()

        return even_clusters

    def kmeans(self,
               n_clusters: int,
               n_init: int = 10,
               max_iter: int = 300,
               seed: int = 42) -> Tuple[Optional[np.ndarray], Optional[KMeans]]:
        """k-means algorithm to cluster points based on their euclidian distance using the sum-of-squares criterion

        This algorithm uses the sklearn python library
        See the documentation here: https://scikit-learn.org/stable/modules/generated/sklearn.cluster.KMeans.html

        Args:
            n_clusters: Number of clusters to form as well as the number of centroids to generate.
            n_init: Number of time the k-means algorithm will be run with different centroid seeds.
                    The final results will be the best output of `n_init` consecutive runs in terms of inertia.
            max_iter: Maximum number of iterations of the k-means algorithm for a single run.
            seed: Determines random number generation for centroid initialization.

        Returns:
            List of indices of the cluster each sample belongs to and fitted KMeans model.

        Raises:
            RuntimeError: If called before points have been set.

        Examples:
            Trivial example in 1D:
            >>> finder = BestPairsFinder(BestPairsFinder.distance)
            >>> finder.points = [1, 2, 3, 4, 5, 6]
            >>> clusters, _ = finder.kmeans(2)
            >>> clusters
            [1, 1, 1, 0, 0, 0]
        """
        if self._points is None:
            raise RuntimeError("points have not yet been set!")

        if self.n_points == 0:
            return None, None

        # prepare points for application of sklearn.KMeans
        if not hasattr(self._points[0], '__len__'):
            points = np.array(self._points).reshape(-1, 1)
        else:
            points = np.array(self._points)

        # init KMeans
        km = KMeans(init='k-means++',
                    n_clusters=n_clusters,
                    n_init=n_init,
                    max_iter=max_iter,
                    random_state=seed)

        # fit km model and predict clusters
        cluster = km.fit_predict(points)

        return cluster, km

    def cluster_and_solve(self, n_clusters: int) -> Tuple[List[Tuple[int, int]], float]:
        """Form even-sized clusters using the K-Means algorithm as used in
        `BestPairsFinder.get_even_sized_clusters`, and find the ideal pairing
        in each cluster using the brute-force method, with the FastPairing method
        as a starting guess.

        Args:
            n_clusters: Lower bound for the number of clusters to form.

        Returns:
            Tuple containing a list of optimal pairings, and the
            corresponding target function.
        """
        clusters = self.get_even_sized_clusters(n_clusters)
        cluster_solver = BestPairsFinder(self.f)

        # Create a key to map each point in the clusters to
        # its actual index in the points array, so key[i][j] = k
        # means the j-th point in the i-th cluster is the k-th point
        # in self._points
        key = [[] for _ in range(len(clusters))]
        for i, cluster in enumerate(clusters):
            for cluster_point in cluster:
                for j, point in enumerate(self._points):
                    if np.allclose(point, cluster_point):
                        key[i] += [j]
                        break

        pairs = []
        s = 0.0

        for i, cluster in enumerate(clusters):
            cluster_solver.points = cluster
            _, guess = cluster_solver.find_pairs()

            # For now, let's be satisfied with the first solution
            (cluster_pairs, *_), cluster_sum = cluster_solver.find_pairs_recursive(guess)

            pairs += [(key[i][j], key[i][k]) for (j, k) in cluster_pairs]
            s += cluster_sum

        return sorted(pairs, key=lambda pair: pair[0]), s


if __name__ == "__main__":
    PairFinder = BestPairsFinder(BestPairsFinder.distance)
    PairFinder.points = [-10, -1, -2, 1]
    print(PairFinder.find_pairs())
    print(PairFinder.find_pairs_recursive())

    PairFinder.points = ParticleGenerator.generate_particles(20, 3)

    start = time.perf_counter()
    print(PairFinder.find_pairs())
    print(time.perf_counter() - start)

    start = time.perf_counter()
    print(PairFinder.find_pairs_recursive())
    print(time.perf_counter() - start)

    start = time.perf_counter()
    print(PairFinder.find_pairs_recursive(lowest_guess=PairFinder.find_pairs()[1]))
    print(time.perf_counter() - start)

    start = time.perf_counter()
    print(PairFinder.pairs_MC(PairFinder.find_pairs()[0], T=5, n_moves=1000))
    print(time.perf_counter() - start)

    start = time.perf_counter()
    print(PairFinder.cluster_and_solve(n_clusters=5))
    print(time.perf_counter() - start)

