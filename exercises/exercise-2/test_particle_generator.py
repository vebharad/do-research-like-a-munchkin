#!/usr/bin/env python3
import pytest
from particle_generator import ParticleGenerator


@pytest.mark.parametrize(("n_particles", "expected_length"),
                         [(1, 1),
                          (2, 2),
                          (10, 10)
                          ]
                         )
def test_number_of_particles(n_particles, expected_length):
    particles = ParticleGenerator.generate_particles(n_particles, 1)
    assert len(particles) == expected_length


@pytest.mark.parametrize(("n_dimensions", "expected_dim"),
                         [(1, 1),
                          (2, 2),
                          (10, 10)
                          ]
                         )
def test_number_of_dimensions(n_dimensions, expected_dim):
    particles = ParticleGenerator.generate_particles(10, n_dimensions)
    assert all(particle.shape == (expected_dim,) for particle in particles)


@pytest.mark.parametrize(("min_val", "max_val"),
                         [(0, 1),
                          (-10, 10)
                          ]
                         )
def test_points_in_interval(min_val, max_val):
    particles = ParticleGenerator.generate_particles(10, 3, min_val, max_val)
    assert all(min_val <= coord < max_val for particle in particles for coord in particle)


@pytest.mark.parametrize("n_particles", [-1, 0])
def test_n_particles_not_positive(n_particles):
    with pytest.raises(ValueError):
        ParticleGenerator.generate_particles(n_particles, 1)


@pytest.mark.parametrize("n_dimensions", [-1, 0])
def test_n_dimensions_not_positive(n_dimensions):
    with pytest.raises(ValueError):
        ParticleGenerator.generate_particles(1, n_dimensions)


@pytest.mark.parametrize(("min_val", "max_val"),
                         [(1.0, 1.0),
                          (1.0, 0.0)
                          ]
                         )
def test_min_val_greater_than_or_equal_to_max_val(min_val, max_val):
    with pytest.raises(ValueError):
        ParticleGenerator.generate_particles(1, 1, min_val, max_val)
