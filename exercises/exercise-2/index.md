# Do Research Like a Munchkin - Team 1 - Research Project

This is the documentation of our findings from the research project carried out during the
[Do Research Like a Munchkin](https://the-munchkins.gitlab.io/do-research-like-a-munchkin) programming
workshop.

## The Brute-Force Method
While it certainly does not represent an ideal solution in most cases,
trying to solve a problem by brute force can often reveal some interesting
characteristics about the problem at hand.

Furthermore, if we remain in a realm where the brute-force method is computationally
feasible, it is always guaranteed to provide an exact solution and, with some
modifications, can also detect if there are multiple ways this solution can be
achieved.

In the case of our research project, the brute-force approach is relatively straightforward:
Go over all possible pairings of \(N\) particles (where \(N\) is even) and find the one
that minimizes the target function.

### Just how many possible combinations are there?
Let us consider an ordered list of points \( \{p_1, p_2, p_3, \dotsc, p_{N-1}, p_N\} \).
There are a total of \(N!\) ways in which we can arrange the particles in this ordered list.

If we now group particles into pairs going left to right, we get an ordered list of \(N/2\)
pairs of particles, e.g., \( \{(p_1, p_2), (p_3, p_4), \dotsc, (p_{N-1}, p_N)\} \), which
can be arranged in any of the \((N/2)!\) possible permutations.

Furthermore, any pair \((p_i, p_j)\) can be written in two ways; for \(N/2\) pairs,
this gives us a factor of \(2^{N/2}\).

The total number of combinations is therefore given by

\[
    \frac{N!}{(N/2)!2^{N/2}}.
\]

### How the algorithm works
The algorithm uses a simple recursive definition: Start by making a list of all
particle indices, and mark them all as available. Then, enter the recursive
part:

1.  If there are no more available indices, we have found a new solution:

    If the current \( \sum f \) is identical to the previous minimum, we have
    multiple equivalent solutions and append the new solution to the list of
    pairings. Otherwise, the solution is the new minimum.

2.  Otherwise, take the first available index as the first particle of the new pair
3.  Loop over all remaining available indices:

    Form the new pair and calculate the new target function: If it exceeds
    the current minimum, we do not need to consider this pair; continue
    with the next index from 3.

    Otherwise, add the new pair to the list of pairs, mark them as unavailable,
    and recursively call the function with the updated list of pairs, target
    function, and available indices.

4.  After all remaining indices have been checked, we will have the minimal
    possible solution for our given pairing.

### Efficiency of the algorithm
As the brute-force approach is guaranteed to find the exact minimum, performance
metrics will mainly be determined by its computational expense.

To reduce the amount of combinations which need to be examined, we employ two
additional techniques:

1.  The particles are sorted in decreasing order by the sum
    \( s_i = \sum\limits_{j\neq i} f(p_i, p_j). \) This way, we can make the
    sums of \( f \) grow quicker, which allows us to abort checking certain
    combinations much sooner.
2.  We use the result from the [Fast Pairing Method](#fast-pairing-method) as
    the initial minimum, again reducing the amount of combinations which need to
    be checked.

To investigate these modifications, we compare the "reference" time \( t_\text{ref} \),
i.e., the time taken by the algorithm from the previous section, and compare it to the
respective time \( t \) taken with our modifications. To account for fluctuations due
to spurious CPU loads, the times were all averaged over the same 100 iterations of
randomly generated points.

![Analysis of different brute-force approaches](./Analysis_BruteForce.png)

Evidently, the overhead resulting from sorting the particles results in higher
computation times for a small number of particles, but is quickly outweighed
by the exponential scaling behavior of the method.

Surprisingly, supplying only a good starting guess does not seem to
systematically improve the performance of the method; however, in combination
with the sorting of particles, the cost is reduced substantially at about
5–20% of the original cost for 20 particles, depending on the number of
dimensions.

Another unexpected finding was the dependence of the computational time on the
number of dimensions. Given that the algorithm operates on a pre-computed
\( f \) matrix, it was our expectation that the dimensionality of the problem
should have negligible effects on the computational expense, but the
opposite seems to be the case: As can be seen in the next figure, the average
times for the improved brute-force method increase drastically, indicating that
a far larger amount of possible combinations need to be examined.

![Effect of the dimensionality on the brute-force approach](./Analysis_Dimension.png)

In summary, the findings using this algorithm are:

*   By not investigating "hopeless" combinations any further than needed, using
    a decent starting guess supplied by a cheaper method, and making the sum
    grow as quickly as possible, the brute-force approach can be substantially
    improved compared to a primitive implementation which simply checks all
    possible combinations.
*   The number of dimensions has a far larger effect on the performance than
    we initially expected.
*   Despite the above mentioned improvements, the algorithm becomes infeasibly
    expensive very quickly, so we recommend not using it for more than 20–30
    particles, depending on the number of dimensions.

## Fast Pairing Method
This is a simple algorithm that tries to find the particle
that has the highest value of the objective function \( f \) when paired with any other
and then pairs it with the particle which produces the smallest value of \( f \) when paired with it.
This algorithm will always find a relatively low sum of \( f \) of pairs, however, it does not have to be the optimal pairing.
In the case when a distance measure is used for the function \( f \) and 1-D points this method yields rigorously the best pairing.
### How the algorithm works
The central quantity is the \( f \)-of-pairs-matrix which contains the value of \( f \) for all possible pairs.
The diagonal elements are masked, as they do not represent any real pairing.
Then the sum along each row of the matrix is computed. The algorithm works then as follows:

1. Find the row with the highest sum.
2. In that row find the lowest element.
3. Pair the two particles with one another.
4. Mask all rows and columns in which the two particles appear.
5. Start again with 1 until all particles have been paired.


### Efficiency of the algorithm
![Placeholder](./RandomPairs_vs_Fast.png)
The quality of the pairing found by this algorithm is determined by computing its relative deviation from the optimal result
which in turn is compared to the relative deviation of a random pairing from the optimal pairing.
The relative deviation is computed for 2 to 20 points in 1-D to 4-D.
The values are reported as means over \( 10^4 \) arrangements of points.

One can clearly see that random pairings become "better" with increasing dimensionality.
The authors propose that this has to do with the combinations simply being less different
due to increased flexibility in higher dimensions.
However, the relative error always increases monotonously with the number of points.
In contrast, the fast algorithm is, as stated before, exact in 1-D and
its relative error seems to level off around 10% for many points.
It is hard to draw definite conclusions from the above plot for more than 20 points.
Unfortunately the exact brute-force algorithm becomes exceedingly expensive
such that averages over many runs become very expensive.


## Monte Carlo Approach

Monte Carlo methods are named after the famous casinos as they are built around using random numbers.
We have implemented a Metropolis Monte Carlo method that tries to improve a given pairing by performing random exchanges.
Such a potential swap of indices is accepted according to the Metropolis criterion, i.e.,
the change is always accepted if it constitutes an improvement
or no change regarding the objective function (in our case the sum over \( f(\text{pairs}) \)).
If the potential swap increases the sum, it is only accepted with the probability \(  e^{- \alpha\cdot\Delta} \),
where \( \alpha \) is a scale factor and \( \Delta \) is the change in the objective function.
In canonical molecular simulations, \( \alpha \) is the inverse thermal energy and \( \Delta \) the change in energy.

### How our algorithm works
1. Select randomly two pairs.
2. Select randomly one point/particle/index in each pair.
3. Evaluate \( \sum f \) for the new pairing.
4. Accept the new pairing according to the Metropolis criterion.
5. Keep track of the lowest possible pairing ever visited.
6. Stop the loop after a predetermined number of attempted swaps.

### Efficiency of the algorithm
The MC approach has several parameters which can be tweaked in order to improve its success rate.
In general, there are three important settings:

1. The pairing passed to the algorithm.
   This will start the algorithm at position where the value of \( \sum f \) is closer to the desired value,
   but it might be further removed in pairing space.
2. The "temperature" at which the simulation is carried out.
   A higher "temperature" (lower \( \alpha \)) which scales \( \Delta \) down more strongly leads to a higher acceptance rate.
   This will enable easy escape of local minima,
   but can also carry the simulation away from the desired global minimum.
3. The number of MC steps performed before the algorithm stops.
   A higher number of steps will always increase the chance of finding the global minimum,
   but makes the method more costly.

![Placeholder](./Analysis_MC.png)
In order to analyse the algorithms the relative divergence from the optimal result was computed
and averaged over \( 10^4 \) calculations.
From the analysis presented in the figure, one can draw the following conclusions:

1. A better pairing will start the algorithm at a much lower error, however,
   the [fast pairing](#fast-pairing-method) is already so good that the MC method
   can only improve it so much. Especially for many points the improvement seems
   to be highly coupled to the scaling factor.
2. A high "temperature" does not improve convergence, more the opposite.
   It seems to carry the system away from the desired minimum.
   It is assumed that a temperature of \( T=10 \) nearly accepts all moves,
   and therefore there is little difference to even higher temperatures.
   Lower "temperatures" seem to enhance convergence up to a point where the scaling becomes so strong that the
   simulation becomes trapped in a minimum from which it cannot escape.
3. As assumed, an increasing number of steps improves the pairing.
   However, the number of random swaps needed to find the optimum increases quickly with the number of points.
   Already for 16 points \( 10^4 \) swaps starting from a random pairing
   do not reach on average the quality of the [fast pairing](#fast-pairing-method),
   which is found extremely quickly.


## K-Means Clustering

The K-Means algorithm can be used to cluster points based on their euclidean distance.
The aim of the algorithm is to choose centroids that minimize the inertia, or within cluster sum of squares criterion:

$$\sum_{i=0}^n \min_{\mu_j \in C} (||x_j - \mu_i||^2)$$

This can be used to sort a large number N of points into k smaller clusters, which are easier to handle for
the previously mentioned pair-finding algorithms.

### How the algorithm works
* In the first step initial centroids are chosen. The convergence of the algorithm is highly dependent on this choice.
It is therefore common to run the simulation several times, with different initializations of the centroids.
In order to improve the quality of initial guesses, the 'k-means++' algorithm can be used. This ensures the centroids to
  be distant to each other, leading to better results than purely random initialization.

* The algorithm now loops self-consistently between the following steps, until convergence is reached:
    * assign each sample to its nearest centroid.
    * generate new centroids by taking the mean value of all the samples assigned to each previous centroid.
    * compute the difference between the old and new centroids.

### Resources
All clustering algorithms that you can use are based on the K-Means implementation provided by
the [scikit-learn](https://sklearn.org/modules/clustering.html) python module.

